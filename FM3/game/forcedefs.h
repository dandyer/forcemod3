 // forcedefs.h  -  Azymn
#ifndef __FORCEDEFS_H__
#define __FORCEDEFS_H__

//#include "bg_public.h"

//  ^0 black 1 red 2 green 3 yellow  4 blue 5 cyan  6 purple 7 white

#ifndef FORCE_LEVEL_4
	#define FORCE_LEVEL_4 4
#endif
#ifndef FORCE_LEVEL_5
	#define FORCE_LEVEL_5 5
#endif

#define MODEL_SCALING_FILE		"modelscale.ini"
#define MODEL_CLASSES_FILE		"modelclass.ini"
#define BOT_CLASS_FILE			"botclasses.ini"

#define MAX_FALL_SPEED		-1200

#define NUM_FMOPTS				6

#define MAX_BLADETYPES			17

#define FHEALTIME_1				10000
#define FHEALTIME_2				10000
#define FHEALTIME_3				10000
#define FHEALTIME_4				10000
#define FTEAMHEALTIME_1			10000
#define FTEAMHEALTIME_2			10000
#define FTEAMHEALTIME_3			10000
#define FTEAMHEALTIME_4			10000
#define FHEALTIME				100	//** based on g_forceregenrate now?
#define FSHIELDTIME				75 //** based on g_forceregenrate now?
#define INFINITE_AMMOREGEN_RATE 75 //** based on g_forceregenrate now?
#define SHIELD_REGEN_AMOUNT		1
#define HEALTH_REGENTIME		500 //** based on g_forceregenrate now?

#define GRIPTIME_OFFSET			30000
#define DRAIN_SLOW_DURATION		GRIPTIME_OFFSET+5000		// client is slow for this many msec
#define MAX_MODELSCALE_FILESIZE	32768

#define	FM_SABERDAMAGEBOOST		4.0f
#define MERC_SABERDMGBOOST		4	//** 4
#define MAX_MERC_SABERDMG		1000 //** 100
#define MIN_MERC_SABERDMG		300
#define MAX_RESIST_SABERDMG		200 //** 100
#define FM_GUNDAMAGE_MULT		3.0f
#define FM_BOMBDAMAGE_MULT		3.0f

#define MIN_MODEL_SCALE			0.43f
#define MAX_MODEL_SCALE			1.70f
#define	SCALING_MODIFIER		0.75f		// smaller == less extreme scaling

#define MAX_TROOPSIZE			5

#define HOOKPULLSPEED			700
#define HOOK_RETRACT_LENGTH		10
#define HOOK_LOOSEN_LENGTH		10
#define HOOKFIRESPEED			800	// 800
#define MAX_HOOKLENGTH			2048.0f

#define MAX_KNOCKBACK			200		// 200 - careful or you'll break stuff
#define EXP_KNOCKBACK			1		//**?? do different knockback for each explosive

#define FORCE_CHARGED_LEVEL_MAX	100
#define FORCE_CHARGEUP_TIME		100	// 400
#define FORCECHARGEUP_FADETIME	2000

#define THERMAL_RADIUS_MOD		2
#define THERMAL_DAMAGE_MOD		4

#define HOTSHOT_LUCK			20		// chance to dodge bullet
#define MAX_HOTSHOT_LUCK		90
#define MIN_DODGETIME			1200

#define MAX_CLOAK_SPEED			1.0f	//**1.02	0.1f;
#define MIN_CAMO_NOMOVE			2000	// time to stand still and activate camo
#define MIN_BOTCLOAKVISIBLE		25

#define BULLETTIME_RADIUS		1024
#define BULLETTIME_SPEED		200

#define TRIGGERHAPPY_TIME		0.7f	// percent of wait tween shots
#define TRIGGERCRAZY_TIME		0.4f	// percent of wait tween shots
#define NONSPECIALTY_PENALTY	1.20f	// firing rate penalty for non-default weapons

#define RANCOR_DAMAGE_MOD		0.05

#define MAX_JETPACK_VEL_UP		324	// 256
#define NUM_SBD_ROCKETS			3

#define RADAR_BLIPTIME			1200

#define RICHOCHET_CHANCE		3	// 1 in n+1 chance of richocheting
#define RICHOCHET_CHANCE_REP	6

#define FMINDTRICKATTACKCOST_4	5
#define FLAMEHIT_TIMEBOOST		2000
#define DRAIN_AMMO				3

#define FLAMETHROWER_RADIUS		200	// 300
#define NUM_FLAMEHITS			10

#define MAX_GRIP_DISTANCE_4		512	// 256
#define MAX_TRICK_DISTANCE 		512	// 512

#define SITH_APPRENTICE_MASTERY	150 // 150
#define SITH_LORD_MASTERY		200 // 200
#define JEDI_LORD_MASTERY		250 // 250
#define ANCIENT_MASTERY			350
#define AT_ONE_MASTERY			500

#define MIN_WEAPONBREAK_DAMAGE	40
#define MIN_SABERBREAK_DAMAGE	50
#define MIN_JETPACKBREAK_DAMAGE	80
#define MIN_CLOAKBREAK_DAMAGE	80
#define	JETPACKFUEL_INAIR		1
#define	JETPACKFUEL_THRUST		2
#define	JETPACKFUEL_THRUST2		3
#define	JETPACKFUEL_THRUST3		4
#define F_JETPACKFLIGHTTIME		500	// time tween fuel checks for pack

#define MAXSABERTYPE		8		// max # of different blade types

// *************************  CLASSES  ************************

typedef enum
{
	NONE=0,
		//	JEDI
	WARDEN,
	AURORIAN,
	SENTINEL,
	ANCIENT_ONE,
		//	SITH
	CARDINAL,
	ZEALOT,
	CRYPT_GUARDIAN,
	DARKSAGE,
		//	MERC
	MANDALORIAN,
	BOUNTY_HUNTER,
	HOTSHOT,
	COMMANDO,
		//	MILITARY
	STORMTROOPER,
	SPACETROOPER,
	CLONETROOPER,
	CRIMSON_GUARD,
	SCOUT,
	ARC_TROOPER,
		//	SPECIES
	WOOKIEE,
	TUSKEN_RAIDER,
	BEASTMASTER,	// UNUSED
	NOGHRI_WARRIOR,
	YUUZHAN_VONG,
	GEONOSIAN,		// UNUSED
	EWOK,			// UNUSED
	JAWA,			// UNUSED
	GUNGAN,
		//	DROIDS
	BATTLE_DROID,
	SUPER_BATTLE_DROID,
	DROIDEKA,
	MAGNA_GUARD,
	ASSASSIN_DROID,
	DARKTROOPER,
		//
	NUM_CLASSES
}fmClass;

#define MIN_JEDI_CLASS		WARDEN
#define MIN_SITH_CLASS		CARDINAL
#define MIN_MERC_CLASS		MANDALORIAN
#define MIN_MILITARY_CLASS	STORMTROOPER
#define MIN_SPECIES_CLASS	WOOKIEE
#define MIN_DROID_CLASS		BATTLE_DROID

#define MAX_JEDI_CLASS		MIN_SITH_CLASS
#define MAX_SITH_CLASS		MIN_MERC_CLASS
#define MAX_MERC_CLASS		MIN_MILITARY_CLASS
#define MAX_MILITARY_CLASS	MIN_SPECIES_CLASS
#define MAX_SPECIES_CLASS	MIN_DROID_CLASS
#define MAX_DROID_CLASS		NUM_CLASSES


// ************************ POWERS ***********************

#define	FMF_HEAVYMELEE	 	(1 << 0)	// extra melee damage
#define FMF_FASTFORCEREGEN	(1 << 1)	// regenerate x times as fast
#define FMF_FLAMETHROWER	(1 << 2)	// fp_lightning
#define FMF_STATVIEWER		(1 << 3)	// too laggy?
#define FMF_NOGUNPICKUP		(1 << 4)	// can't pickup additional weapons
#define FMF_GRAPPLE			(1 << 5)
#define FMF_DUALGUNS		(1 << 6)	// if changed change in bg_pmove.c too
#define FMF_CAMOUFLAGE		(1 << 7)	// auto-cloak if slow/still
#define FMF_CUSTOMSKEL		(1 << 8)	// PM_IsRocketTrooper(void)
#define FMF_AMMOREGEN		(1 << 9)	//**^^PM_AddEventWithParm( EV_NOAMMO, WP_NUM_WEAPONS+pm->ps->weapon );
#define FMF_SHIELDREGEN		(1 << 10)
#define FMF_WALLCRAWL		(1 << 11)	// climb around walls - 0 //**?? DISABLED FOR NOW
#define FMF_AUTODODGE		(1 << 12)	// chance to dodge fire
#define FMF_FORCEIMMUNE		(1 << 13)
#define FMF_NPCTROOP		(1 << 14)
#define FMF_TARGETING		(1 << 15)	// extra crosshairs for targeting

#define FMF_RADAR			(1 << 16)
#define	FMF_TRIGGERHAPPY	(1 << 17)
#define	FMF_GUNROLL			(1 << 18)	// can roll with guns
#define FMF_OPTIC_RADAR		(1 << 19)
#define FMF_THERMAL_VISION	(1 << 20)
#define FMF_UNLIMITED_AMMO	(1 << 21)	// constantly regenerates ammo
#define FMF_EXTRA_HEAVY		(1 << 22)	// can't be pushed/pulled
#define FMF_DOUBLE_AMMO		(1 << 23)
#define FMF_SNIPERMOVE		(1 << 24)	// move while zoomed
#define FMF_MULTISENTRY		(1 << 25)	// up to # sentries
#define FMF_BERSERKER		(1 << 26)	// wookiee/tusken rage
#define	FMF_PLATEARMOR		(1 << 27)	// reduces damage
#define FMF_NOGUNSHOW		(1 << 28)
#define FMF_MARTIALARTS		(1 << 29)	// allowed to use grapple moves/kicks
#define FMF_HIGHJUMP		(1 << 30)	// level 1 forcejump
#define FMF_AUTOHEALTHREGEN	(1 << 31)	// regenerate health constantly

//**  FMF2 Flags

#define FMF2_SPECIALSABER	(1 << 0)	// use custom, forced saber
#define FMF2_LIFELEECH		(1 << 1)	// life leech ability
#define	FMF2_WEAK_PHYSICAL	(1 << 2)	// take extra melee damage
#define FMF2_SABER_RESIST	(1 << 3)	// sabers do half damage
#define FMF2_EXPLOSION_RESIST (1 << 4)	// 1/2 damage from explosives
#define FMF2_GUN_RESIST	 	(1 << 5)	// 1/2 damage from guns
#define FMF2_TRIGGERCRAZY	(1 << 6)
#define FMF2_NOFALLDAMAGE	(1 << 7)
#define	FMF2_BATTLESPEED	(1 << 8)	// force speed, basically
#define	FMF2_WEAK_DEMP2		(1 << 9)
#define FMF2_MELEE_RESIST 	(1 << 10)	// 1/3 melee damage
#define FMF2_LEDGEGRABBER	(1 << 11)	


enum
{
	QUICK_GETUP=1,
	SPRING_GETUP,
	JUMPKICK,
	HEADLOCK
}; //** martial arts moves

// ************************ EQUIPMENT ***********************

/*

	WP_STUN_BATON,
	WP_MELEE,
	WP_SABER,
	WP_BRYAR_PISTOL,
	WP_BLASTER,
	WP_DISRUPTOR,
	WP_BOWCASTER,
	WP_REPEATER,
	WP_DEMP2,
	WP_FLECHETTE,
	WP_ROCKET_LAUNCHER,
	WP_THERMAL,
	WP_TRIP_MINE,
	WP_DET_PACK,
	WP_CONCUSSION,
	WP_BRYAR_OLD,
	WP_EMPLACED_GUN,
	WP_TURRET,

#define HIF_SEEKER			(1 << HI_SEEKER)
#define HIF_SHIELD 			(1 << HI_SHIELD)
#define HIF_SENTRY_GUN		(1 << HI_SENTRY_GUN)
#define HIF_HEALTHDISP		(1 << HI_HEALTHDISP)
#define HIF_AMMODISP		(1 << HI_AMMODISP)
#define HIF_EWEB 			(1 << HI_EWEB)
#define HIF_BINOCULARS 		(1 << HI_BINOCULARS)
#define HIF_JETPACK 		(1 << HI_JETPACK)
#define HIF_MEDPAC_BIG		(1 << HI_MEDPAC_BIG)
#define HIF_MEDPAC 			(1 << HI_MEDPAC)
#define HIF_CLOAK 			(1 << HI_CLOAK)
*/


#endif // END FORCEDEFS.H


